There is a start-db.bat file provided, which will start the db in server mode. to start it, go to the home folder where the project is place and run the bat file. This is to validate the db when the application is running.

If the db is not started that way, then, the application will start by itself in file in-memory mode. 
 