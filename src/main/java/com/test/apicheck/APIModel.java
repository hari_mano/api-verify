package com.test.apicheck;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "API_KEY")
public class APIModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;

	@Column(name = "KEY")
	private String key;

	@Column(name = "CONSUMED_ON")
	private LocalDateTime consumedOn;

	@Column(name = "CONSUMED")
	private Boolean consumed;

	public void setAPIConsumed() {
		this.consumed = true;
		this.consumedOn = LocalDateTime.now();
	}

}
