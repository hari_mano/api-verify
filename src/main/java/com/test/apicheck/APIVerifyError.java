package com.test.apicheck;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APIVerifyError {
	private String message;

}
