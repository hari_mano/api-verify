package com.test.apicheck.resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.test.apicheck.APIKeyManagementRepository;
import com.test.apicheck.APIKeyVerificationModel;
import com.test.apicheck.APIModel;
import com.test.apicheck.exceptions.KeyNotFoundException;

@RestController
public class APIManagementController {

	@Autowired
	private APIKeyManagementRepository apiKeyManagementRepository;

	@Value("${apiKey.length}")
	private int apiKeyLength;

	@GetMapping(value = "duplicatecheck")
	public APIKeyVerificationModel verifyAPIKey(@RequestParam(name = "apiKey") String apiKey) {

		Validate.notEmpty(apiKey, "ApiKey cannot be empty");
		Validate.isTrue(StringUtils.length(apiKey) == apiKeyLength, "apiKey must be of size " + apiKeyLength);

		APIModel apiKeyModel = apiKeyManagementRepository.findByKey(apiKey);
		if (apiKeyModel == null) {
			throw new KeyNotFoundException(apiKey);
		}

		Boolean consumed = apiKeyModel.getConsumed();
		if (consumed != null && consumed) {
			return new APIKeyVerificationModel(apiKey, true);
		}

		apiKeyModel.setAPIConsumed();
		apiKeyManagementRepository.save(apiKeyModel);
		return new APIKeyVerificationModel(apiKey, false);
	}

}
