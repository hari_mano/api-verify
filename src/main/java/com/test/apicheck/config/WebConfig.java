package com.test.apicheck.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class WebConfig implements WebMvcConfigurer {

	@Value("${spring.datasource.server-url}")
	private String url;

	@Value("${spring.datasource.file-url}")
	private String fileUrl;

	@Value("${spring.datasource.username}")
	private String username;

	@Value("${spring.datasource.password}")
	private String password;

	@Bean
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource.hikari")
	public DataSource dataSource() {
		log.info("Initializing datasource....");

		DataSource ds = null;
		try {
			ds = DataSourceBuilder.create().type(com.zaxxer.hikari.HikariDataSource.class).url(url).username(username)
					.password(password).build();
			/**
			 * test connection
			 */
			ds.getConnection();
		} catch (Exception e) {
			ds = DataSourceBuilder.create().type(com.zaxxer.hikari.HikariDataSource.class).url(fileUrl)
					.username(username).password(password).build();
		}

		return ds;
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**");
	}
}