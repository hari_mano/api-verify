package com.test.apicheck.exceptions;

public class KeyNotFoundException extends DataInputException {

	private static final long serialVersionUID = 7566787617316776554L;

	private String apiKey;

	public KeyNotFoundException(String apiKey) {
		this.apiKey = apiKey;
	}

	@Override
	public String getMessage() {
		return "Key provided is not found in the datastore - " + apiKey;
	}

}
