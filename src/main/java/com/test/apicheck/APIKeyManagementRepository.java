package com.test.apicheck;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@Repository
public interface APIKeyManagementRepository extends JpaRepository<APIModel, Long> {

	APIModel findByKeyAndConsumed(String key, Boolean consumed);
	
	APIModel findByKey(String key);

}