package com.test.apicheck;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class APIKeyVerificationModel {

	private String apiKey;
	private Boolean result;

	public APIKeyVerificationModel(String apiKey, Boolean result) {
		this.apiKey = apiKey;
		this.result = result;
	}

}
