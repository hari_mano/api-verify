package com.test.apicheck;

import org.apache.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerAdvice {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<APIVerifyError> handleException(Exception ex) {
		APIVerifyError response = new APIVerifyError();
		response.setMessage(ex.getMessage());
		return ResponseEntity.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).body(response);
	}

}
