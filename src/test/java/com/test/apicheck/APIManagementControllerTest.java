package com.test.apicheck;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.test.apicheck.resource.APIManagementController;

@RunWith(SpringRunner.class)
@WebMvcTest(value = APIManagementController.class)
@AutoConfigureJsonTesters
public class APIManagementControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Value("${apiKey.length}")
	private int apiKeyLength;

	String validApiKey = "11111111111111111111";

	@MockBean
	private APIKeyManagementRepository apiKeyManagementRepository;

	@Autowired
	private APIManagementController apiManagementController;

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Autowired
	private JacksonTester<APIKeyVerificationModel> json;

	@Test
	public void emptyInputAPIKeyTest() {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("ApiKey cannot be empty");
		String apiKey = "";
		apiManagementController.verifyAPIKey(apiKey);

	}

	@Test
	public void inputLengthAPIKeyTest() {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("apiKey must be of size " + apiKeyLength);
		String apiKey = "abcdefghijklmn";
		apiManagementController.verifyAPIKey(apiKey);
	}

	@Test
	public void APIKeyFirstTimeTest() throws Exception {

		String uri = "/duplicatecheck";

		APIModel apiModel = new APIModel();
		apiModel.setKey(validApiKey);
		apiModel.setConsumed(false);

		Mockito.when(apiKeyManagementRepository.findByKey(validApiKey)).thenReturn(apiModel);

		MvcResult mvcResult = mockMvc.perform(
				MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE).param("apiKey", validApiKey))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		assertThat(this.json.parseObject(content).getApiKey()).isEqualTo(validApiKey);
		assertThat(this.json.parseObject(content).getResult()).isEqualTo(false);
	}

	@Test
	public void APIKeyRepeatTest() throws Exception {

		String uri = "/duplicatecheck";

		APIModel apiModel = new APIModel();
		apiModel.setKey(validApiKey);
		apiModel.setConsumed(true);

		Mockito.when(apiKeyManagementRepository.findByKey(validApiKey)).thenReturn(apiModel);

		MvcResult mvcResult = mockMvc.perform(
				MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE).param("apiKey", validApiKey))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		assertThat(this.json.parseObject(content).getApiKey()).isEqualTo(validApiKey);
		assertThat(this.json.parseObject(content).getResult()).isEqualTo(true);
	}

}
